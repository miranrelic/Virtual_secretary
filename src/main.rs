#[macro_use]
extern crate log;

use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use chrono::{NaiveDateTime, Utc};
use dotenv::dotenv;
use std::env;
use minicaldav::{save_event, Calendar, Event};
use serde::{Deserialize, Serialize};
use tera::{Context, Tera};
use url::Url;
use uuid::Uuid;


#[derive(Debug, Deserialize, Serialize)]
struct EventInquiry {
    name: String,
    summary: String,
    description: String,
    start_time: String,
    end_time: String,
    time_zone: String
}

async fn inquiry(tera: web::Data<Tera>) -> impl Responder {
    let mut data = Context::new();
    data.insert("title", "Send Event Inquiry");

    let rendered = tera.render("index.html", &data).unwrap();
    HttpResponse::Ok().body(rendered)
}

async fn process_inquiry(tera: web::Data<Tera>, data: web::Form<EventInquiry>) -> impl Responder {
    // println!("{:?}", data);
    let new_data = data.into_inner();
    match add_event(&new_data) {
        Ok(_) => {
            let mut data = Context::from_serialize(new_data).unwrap();
            data.insert("title", "Inquiry Sent Succesfully");
            println!("{:?}", data);
            let rendered = tera.render("success.html", &data).unwrap();
            HttpResponse::Ok().body(rendered)},
        Err(_) => HttpResponse::Ok().body(format!(
            "Failed to send inquiry for event: {}",
            new_data.summary
        )),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    HttpServer::new(|| {
        let tera = Tera::new("templates/**/*").unwrap();
        App::new()
            .service(fs::Files::new("/static", "./static"))
            .app_data(web::Data::new(tera))
            .route("/", web::get().to(inquiry))
            .route("/", web::post().to(process_inquiry))
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await
}

fn add_event(inquiry: &EventInquiry) -> Result<(), ()> {
    dotenv().ok();
    let agent = ureq::Agent::new();
    let url = Url::parse(&env::var("CALDAV_URL").unwrap()).unwrap();
    let username = &env::var("CALDAV_USER").unwrap();
    let password = &env::var("CALDAV_PASS").unwrap();
    let calendars = minicaldav::get_calendars(agent.clone(), username, password, &url).unwrap();
    let mut personal_calendar: Option<Calendar> = None;
    for calendar in calendars {
        if calendar.name() == "Personal" {
            personal_calendar = calendar.into();
        }
    }
    match personal_calendar {
        Some(calendar) => {
            let summary = format!("{} - {}", inquiry.name, inquiry.summary);
            let description = inquiry.description.replace("\r\n", "\\n");
            let timestamp = Utc::now().format("%Y%m%dT%H%M%SZ").to_string();
            let start_time = NaiveDateTime::parse_from_str(
                format!("{}", inquiry.start_time).as_str(),
                "%Y-%m-%d %H:%M",
            )
            .unwrap()
            .format("%Y%m%dT%H%M%S")
            .to_string();
            let end_time = NaiveDateTime::parse_from_str(
                format!("{}", inquiry.end_time).as_str(),
                "%Y-%m-%d %H:%M",
            )
            .unwrap()
            .format("%Y%m%dT%H%M%S")
            .to_string();
            let new_event = Event::builder(
                Url::parse(format!("{}{}.ics", calendar.url(), Uuid::new_v4(),).as_str()).unwrap(),
            )
            .summary(summary)
            .timestamp(timestamp)
            .description(Some(description))
            .start(start_time, vec![("TZID", "Europe/Zagreb")])
            .end(end_time, vec![("TZID", "Europe/Zagreb")])
            .build();
            match save_event(agent, username, password, new_event) {
                Ok(ev) => println!("{:?}", ev),
                Err(err) => println!("{:?}", err),
            }
            Ok(())
        }
        None => {
            error!("Personal calendar not found");
            Err(())
        }
    }
}
