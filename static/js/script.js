
function check_times() {
    dateStart = document.getElementsByName("start_time")[0].value;
    dateEnd = document.getElementsByName("end_time")[0].value;
    if (dateStart == "" || dateEnd == "" || Date.parse(dateStart) > Date.parse(dateEnd)) {
        alert("Incorrect dates");
        return false;
    }
    form = document.getElementById('inquiry_form');
    input = document.createElement('input');
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    input.setAttribute("type", "text");
    input.setAttribute("name", "time_zone");
    input.setAttribute("value", timezone);
    input.style.display = "none";
    form.appendChild(input);

    form.submit();
}


config = {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
    minDate: "today",
    time_24hr: true,
    "locale": {
        "firstDayOfWeek": 1
    }
}
flatpickr(".datetime-picker", config);

const textarea_resize = (e) => {
    e.target.style.height = '20px';
    e.target.style.height = e.target.scrollHeight - offset + 'px';
}

function init() {
    description_textarea = document.getElementById("description_textarea");
    offset = 17;
    description_textarea.value = "";

    description_textarea.addEventListener('input', textarea_resize);
}

window.onload = init;